#######################
Stereo width controller
#######################

Overview
========

A stereo width controller using the ancient / discontinued
TDA3810 IC.

.. image:: doc/tda3810/tda3810-stereo-width-controller.jpg

Status
======

Project finished, two devices have been built.

Licensing
=========

CC BY-SA for all files in ``hardware`` subfolder

Copyright
=========

For all files in ``hardware`` folder copyright is
(c) 2021 Andreas Messer <andi@bastelmap.de>.



